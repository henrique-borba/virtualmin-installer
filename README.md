# Virtualmin Installer

Bootstrap for [Virtualmin](https://www.virtualmin.com/), for an optimal setup for WikiSuite components, especially [Tiki Manager](https://gitlab.com/tikiwiki/tiki-manager) and [Tiki Wiki CMS Groupware](https://gitlab.com/tikiwiki/tiki).

This reuses the http://software.virtualmin.com/gpl/scripts/install.sh script at https://virtualmin.com/download and will make opiniated choices that make sense for running Tiki instances. For example, it will install Tiki dependencies and multiple versions of PHP. Differences are explained here: https://wikisuite.org/Differences-from-the-original-script After running the script, you can configure Webmin/Virtualmin as usual. 

# Requirements

- Tested with Debian 10 and Ubuntu 20.04 LTS.
- Should work for Ubuntu 18.04 LTS and Debian 9.
- CentOS 7.x support is coming soon.

# How to use
All you need to do is run this script as root on a fresh install of Linux:

```
curl -o install-virtualmin.sh https://gitlab.com/wikisuite/virtualmin-installer/-/raw/master/install-virtualmin.sh
bash install-virtualmin.sh
```

After running the script, you can continue with instructions from https://www.virtualmin.com/download.html (GUI is at port 10000)

# Related links

- https://wikisuite.org/Virtualmin
